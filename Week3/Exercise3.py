import numpy as np

def enterNumber(prompt, reminder = "Please enter 3 numbers (ex: 2 3 4)"):
    while True:
        try:
            inputs = input(prompt).split(" ")
            m, n, k = int(inputs[0]), int(inputs[1]), int(inputs[2])
            return m, n, k
        except :
            print(reminder)

m, n, k = enterNumber("enter 3 numbers dept width height (ex: 2 3 4):")
#reate 3D array M x N x K with random integer numbers on interval [-100,100]
arr = np.random.randint(-100, 101, m * n *k).reshape(m, n, k)
print(arr)
print('-------------------------------------')
print("Max in deep: \n", arr.max(axis = 0))
print("Max in width: \n", arr.max(axis = 2))
print("Max in height: \n", arr.max(axis = 1))

print('-------------------------------------')

print("Min in deep: \n", arr.min(axis = 0))
print("Min in width: \n", arr.min(axis = 2))
print("Min in height: \n", arr.min(axis = 1))

print('-------------------------------------')

print("Sum in deep: \n", arr.sum(axis = 0))
print("Sum in width: \n", arr.sum(axis = 2))
print("Sum in height: \n", arr.sum(axis = 1))