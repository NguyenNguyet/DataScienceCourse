import numpy as np

def enterNumber(prompt, reminder = "Please enter a number!"):
    while True:
        try:
            n = int(input(prompt))
            return n
        except ValueError:
            print(reminder)

n = enterNumber("Enter a number:")
#create 3D array N x N x N with random (float) values on interval [0,10]
arr = np.random.uniform(0, 11, n**3).reshape(n, n, n)
print(arr)

print('-------------------------------------')

print("Max in deep: \n", arr.max(axis = 0))
print("Max in width: \n", arr.max(axis = 2))
print("Max in height: \n", arr.max(axis = 1))

print('-------------------------------------')

print("Min in deep: \n", arr.min(axis = 0))
print("Min in width: \n", arr.min(axis = 2))
print("Min in height: \n", arr.min(axis = 1))

print('-------------------------------------')

print("Sum in deep: \n", arr.sum(axis = 0))
print("Sum in width: \n", arr.sum(axis = 2))
print("Sum in height: \n", arr.sum(axis = 1))