#What is the future value after 10 years of saving $100 now, with an additional monthly savings of $100.
# Assume the interest rate is 5% (annually) compounded monthly?
import numpy as np
futureMoney = np.fv(0.05/12, 10*12, -100, -100)
print(futureMoney)

#What is the present value (e.g., the initial investment) of an investment that needs to total $15692.93 after 10 years of saving $100 every month?
# Assume the interest rate is 5% (annually) compounded monthly.
presentMoney = np.pv(0.05/12, 10 *12, -100, 15692.93)
print(presentMoney)

#Returns the NPV (Net Present Value) of a cash flow series.
print(np.npv(0.281,[-100, 39, 59, 55, 20]))

#What is the monthly payment needed to pay off a $200,000 loan in 15 years at an annual interest rate of 7.5%?
print(np.pmt(0.075/12, 12*15, 200000))