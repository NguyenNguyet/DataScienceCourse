import numpy as np
iris = np.genfromtxt("iris.txt", delimiter=",", dtype=None, names=('sepal length', 'sepal width', 'petal length', 'petal width', 'label'), encoding='utf-8')
#print(iris)  #list of tuples
dict = {'Iris-setosa' : 0 , 'Iris-versicolor' : 1, 'Iris-virginica' : 2}
for item in iris:
    item[4] = dict.get(item[4], -1)
arr = np.array(iris.tolist(), dtype = np.float16)
print(arr)

