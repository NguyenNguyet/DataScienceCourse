import numpy as np
#1. Let's create a numpy array from a list.
x = range(1, 11) #tạo ra 1 list
a1 = np.array(x, 'i') #tạo ra 1 mảng int từ list
print('a1 =', a1)
print(a1.dtype)
a2 = np.array(x, 'f') #tạo ra 1 mảng float từ list
print('a2 =', a2)
print(a2.dtype)

#2. Let's create arrays in different ways.
x1 = np.zeros((2, 3, 4))
print('x1 = \n', x1)
x2 = np.ones((2, 3, 4))
print('x2 =\n', x2)
x3 = np.arange(1000) #mảng từ 0 -> 999


#3. Let's look at indexing and slicing arrays.
a = np.array([2, 3.2, 5.5, -6.4, -2.2, 2.4] )
print(a)
print('a[1] =', a[1])
print('a[1:4] = ', a[1:4])
a = np.array([[2, 3.2, 5.5, -6.4, -2.2, 2.4],
              [1, 22, 4, 0.1, 5.3, -9],
              [3, 1, 2.1, 21, 1.1, -2]])
print(a)
print('a[:, 3] =\n', a[:, 3]) #cột 3
print('a[1:4, 0:4] =\n', a[1:4, 0:4]) #hàng  1, 2 ; cột  0, 1, 2, 3
print('a[1:, 2] =', a[1:,2]) #hàng 1 trở đi, cột 2

print('\n-----------------')
print('-----------------')
#1. Let's interrogate an array to find out its characteristics
arr = np.array([range(4), range(10,14)])
print(arr)
print(arr.shape)
print(arr.size) #Kích thước của mảng xem có bao nhiêu phần tử tất cả
print('max =', arr.max())
print('min =', arr.min())
#2. Let's generate new arrays by modifying our array.
print(arr.reshape(2, 2, 2))
print(arr.T) #ma trận chuyển vị
print(arr.ravel()) #ma trận phẳng 1 chiều
print(arr.astype('f')) #in mảng được convert thành float

print('\n-----------------')
print('-----------------')

#Exercise: Array calculations and operations
#Aim:  Use NumPy arrays in mathematical calculations
#1. Let's perform some array calculations.
a = np.array([range(4), range(10, 14)])
print('a =', a)
b = np.array([2, -1, 1, 0])
print('b =',b)
print('a * b =', a * b)  #broadcasting: b chuyển thành mảng 2 chiều, copy lại row cũ
b1 = b * 100
print('b1 =', b1)
b2 = b * 100.0
print('b2 =', b2)
print('b1 == b2 ? :', b1 == b2) #[ True  True  True  True]
print('b1.dtype =', b1.dtype)
print('b2.dtype =', b2.dtype)

print('-------------------------\n\n')
#2. Let's look at array comparisons.
arr = np.arange(0, 10)
print('arr =', arr)
print(arr < 3) #so sánh từng phần tử của mảng với 3
print(np.less(arr, 3))
condition = np.logical_or(arr < 3, arr > 8) #Các phần tử từ 3 -> 8 sẽ false
new_arr = np.where(condition, arr * 5, arr * -5)
print(new_arr)

print('-------------------------\n\n')
#3. Let'simplement a mathematical function that works on arrays
def calcMagnitude(u, v, minma = 0.1):
    magnitude = ((u ** 2) + (v ** 2)) ** 0.5
    magnitude = np.where(magnitude > 0.1, magnitude, 0.1)
    return magnitude

u = np.array([[4, 5, 6], [2, 3, 4]])
v = np.array([[2, 2, 2], [1, 1, 1]])
magnitude = calcMagnitude(u, v)
print(magnitude)

u = np.array([[4, 5, 0.01], [2, 3, 4]])
v = np.array([[2, 2, 0.03], [1, 1, 1]])
magnitude = calcMagnitude(u, v)
print(magnitude)

print('--------------------------------------------------------------')
print('--------------------------------------------------------------\n\n')
#Exercise: Working with missing values
#Aim: An introduction to masked arrays to represent missing values
#1.    Let's create a masked array and play with it.
import numpy.ma as MA
marr = MA.masked_array(range(10), fill_value = -999)
print(marr, marr.fill_value)
marr[2] = MA.masked;
print('marr =', marr)
print('marr.mask =', marr.mask)

narr = MA.masked_where(marr > 6, marr)
print('narr =', narr)
print('narr.fill_value =', narr.fill_value)

x = MA.filled(narr)
print('x =', x)
print('type of x: ', type(x))


print('-------------------------\n\n')
#2. Let's create a mask that is smaller than the overall array
m1 = MA.masked_array(range(1,9))
print('m1 =', m1)
m2 = m1.reshape(2, 4)
print('m2 =', m2)
m3 = MA.masked_greater(m2, 6)
print(m3)
m3 = m3 * 100
print(m3)
res = m3 - np.ones((2,4))
print('res =', res)
print('type(res) =', type(res))