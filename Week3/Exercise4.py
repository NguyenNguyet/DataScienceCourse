import numpy as np

def enterNumber(prompt, reminder = "Please enter a number!"):
    while True:
        try:
            n = int(input(prompt))
            return n
        except ValueError:
            print(reminder)

arr = np.random.uniform(-10, 11, 10 * 8).reshape(10, 8)
print(arr)

n = enterNumber("Enter a number:")
arrAfterSubs = np.abs(arr - 5) #tính trị tuyệt đối của mảng đã trừ đi n

print("Nearest value with", n, "in arrays:", arr.flat[arrAfterSubs.argmin()]) #argmin trả về index của phần tử bé nhất

arrIndexSorted = np.argsort(arrAfterSubs, axis = None) #Trả về danh sách chỉ số theo value đã sắ = np.argsort(arrAfterSubs, axis = None) #Trả về danh sách chỉ số theo value đã sắIndex = np.argsort(arrAfterSubs, axis = None) #Trả về danh sách chỉ số theo value đã sắ = np.argsort(arrAfterSubs, axis = None) #Trả về danh sách chỉ số theo value đã sắIndexp xếp
print("3 nearest values with", n, "in arrays:", arr.flat[arrIndexSorted[0]], arr.flat[arrIndexSorted[1]], arr.flat[arrIndexSorted[2]])