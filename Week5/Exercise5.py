# Write your answer from here, add more cells if needed
import numpy as np
import pandas as pd

data = pd.read_csv('customers.csv')
data = data.drop(columns=['Channel', 'Region', 'Frozen', 'Detergents_Paper', 'Delicatessen'])
print(data.head())
log_data = np.log(data)  # logarit co so e
print(log_data.head())

pd.plotting.scatter_matrix(data, diagonal='kde', figsize=(14, 8))

outliersIndex = []
for feature in 'Grocery', 'Milk', 'Grocery':
    Q1 = np.percentile(data[feature], 25)
    Q3 = np.percentile(data[feature], 75)
    step = (Q3 - Q1) * 1.5
    outliers = data[~((Q1 - step <= data[feature]) & (data[feature] <= Q3 + step))]
    outliersIndex += outliers.index.tolist()

goodData = data.drop(data.index[outliersIndex]).reset_index(drop=True)
pd.plotting.scatter_matrix(goodData, diagonal='kde', figsize=(14, 8))