# Write your answer from here, add more cells if needed
import numpy as np
import pandas as pd

data = pd.read_csv('customers.csv')
data = data.drop(columns=['Channel', 'Region', 'Frozen', 'Detergents_Paper', 'Delicatessen'])

import scipy.stats as ss

c1, p1 = ss.pearsonr(data.Grocery, data.Milk)
print("p-value for non-correlation of ('Grocery', 'Milk'):", p1)
c2, p2 = ss.pearsonr(data.Grocery, data.Fresh)
print("p-value for non-correlation of ('Grocery', 'Fresh'):", p2)
c3, p3 = ss.pearsonr(data.Fresh, data.Milk)
print("p-value for non-correlation of ('Fresh', 'Milk'):", p3)
a = 0.1
if p1 < a:
    print("Accept H = 'Grocery' and 'Milk' are corellated.")
else:
    print("Reject H = 'Grocery' and 'Milk' are corellated.")

if p2 < a:
    print("Accept H = 'Grocery' and 'Fresh' are corellated.")
else:
    print("Reject H = 'Grocery' and 'Fresh' are corellated.")

if p3 < a:
    print("Accept H = 'Fresh' and 'Milk' are corellated.")
else:
    print("Reject H = 'Fresh' and 'Milk' are corellated.")