# Write your answer from here, add more cells if needed
import numpy as np
import pandas as pd


# Write your code from here, add more cells if needed
data = pd.read_csv('customers.csv')
data = data.drop(columns=['Channel', 'Region', 'Frozen', 'Detergents_Paper', 'Delicatessen'])

corr = np.corrcoef(data.Grocery, data.Milk)
print(corr)
print("correlation of 'Grocery' and 'Milk':", corr[0][1])
print('-------------------------------------')
import matplotlib.pyplot as plt
plt.scatter(data.Grocery, data.Milk)
plt.xlabel('Grocery')
plt.ylabel('Milk')
plt.title("scatter plot of 'Grocery' and 'Milk'")
plt.show()

print('-------------------------------------\n\n')
import seaborn as sb
sb.heatmap(data.corr(), annot=True)

print('-------------------------------------')
pd.plotting.scatter_matrix(data, figsize=(14, 8), diagonal='kde')
