# Write your answer from here, add more cells if needed
import numpy as np
import pandas as pd

data = pd.read_csv('customers.csv')
data = data.drop(columns=['Channel', 'Region', 'Frozen', 'Detergents_Paper', 'Delicatessen'])

log_data = np.log(data)  # logarit co so e

outliersIndex = []
for feature in 'Grocery', 'Milk', 'Grocery':
    Q1 = np.percentile(data[feature], 25)
    Q3 = np.percentile(data[feature], 75)
    step = (Q3 - Q1) * 1.5
    outliers = data[~((Q1 - step <= data[feature]) & (data[feature] <= Q3 + step))]
    outliersIndex += outliers.index.tolist()

goodData = data.drop(data.index[outliersIndex]).reset_index(drop=True)

# one-sample-t-test
a1 = 0.05
import scipy.stats as ss

t = ss.ttest_1samp(log_data['Fresh'], 8.8)
if (t[1] < a1):
    print('Accept: The mean of "Fresh" population (after log-transform) by using the log_data set is different 8.8')
else:
    print('Reject: The mean of "Fresh" population (after log-transform) by using the log_data set is different 8.8')

tg = ss.ttest_1samp(goodData['Fresh'], 8.8)
if (tg[1] < a1):
    print(
        'Accept: The mean of "Fresh" population (after log-transform) by using the good_log_data set is differenct 8.8')
else:
    print(
        'Reject: The mean of "Fresh" population (after log-transform) by using the good_log_data set is differenct 8.8')

# two-sample-t-test

t2 = ss.ttest_ind(log_data['Fresh'], goodData['Fresh'])
if (t2[1] < a1):
    print("Reject: The means of 'Fresh' in the log_data and good_log_data are different")
else:
    print("Accept: The means of 'Fresh' in the log_data and good_log_data are different")

tm2 = ss.ttest_ind(log_data['Milk'], goodData['Milk'])
if (t2[1] < a1):
    print("Reject: The means of 'Milk' in the log_data and good_log_data are different")
else:
    print("Accept: The means of 'Milk' in the log_data and good_log_data are different")