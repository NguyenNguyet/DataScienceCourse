import pandas as pd
import numpy as np
# Write your code from here, add more cells if needed
data = pd.read_csv('customers.csv')
print(data.head())
print('Shape:',data.shape)
print('\nDtypes:\n', data.dtypes)
print('\nColumns:', data.columns.tolist())

print('-------------------------------------')
data.Channel = data.Channel.astype(np.float).fillna(0.0)
data.Region = data.Region.astype(np.float).fillna(0.0)
print('Dtypes:\n', data.dtypes)

print('-------------------------------------')
data = data.drop(columns = ['Channel', 'Region', 'Frozen', 'Detergents_Paper', 'Delicatessen'])
print(data.head())