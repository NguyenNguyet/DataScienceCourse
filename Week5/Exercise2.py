import pandas as pd
import numpy as np

# Write your code from here, add more cells if needed
data = pd.read_csv('customers.csv')
data = data.drop(columns = ['Channel', 'Region', 'Frozen', 'Detergents_Paper', 'Delicatessen'])

print(data.Grocery.describe())
print('-------------------------------------')
print('min =', np.min(data.Grocery))
print('max =', np.max(data.Grocery))
print('Q1 =', np.percentile(data.Grocery, 25))
print('median =', np.median(data.Grocery))
print('Q3 =', np.percentile(data.Grocery, 75))
print('-------------------------------------')
print('mean =', np.mean(data.Grocery))
print('range = max - min =', data.Grocery.max() - data.Grocery.min())
import statistics as stt
print('standard deviation = ', stt.stdev(data.Grocery))
import scipy.stats as ss
print('skewness =', ss.skew(data.Grocery))
print('kurtosis =', ss.kurtosis(data.Grocery))
print('-------------------------------------')
import matplotlib.pyplot as plt
plt.boxplot(data.Grocery)
plt.show()
print('-------------------------------------')
plt.hist(data.Grocery)
plt.xlabel('annual spending (m.u.) on grocery products')
plt.ylabel('frequency')
plt.show()