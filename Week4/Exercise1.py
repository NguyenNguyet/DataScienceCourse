# Ex1
import pandas as pd

s = 'Ah meta descriptions… the last bastion of traditional marketing! The only cross-over point between marketing and search engine optimisation! The knife edge between beautiful branding and an online suicide note!'
l = s.split()

series = pd.Series(l)  # Tạo Series từ list


# tạo hàm đếm số lượng nguyên âm của 1 từ, nếu số nguyên âm >=3 thì return True, ngược lại, return False
def countVowel(x):
    count = 0
    for c in x:
        if c in ['A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u']:
            count += 1
    if count >= 3:
        return True
    else:
        return False


# Tạo 1 mask là boolean vector để biết từ nào tại index nào sẽ có số nguyên âm >=3 và từ nào sẽ <3
mask = series.map(lambda x: countVowel(x))

# Trích xuất ra các từ có ít nhất 3 nguyên âm
print(series[mask])