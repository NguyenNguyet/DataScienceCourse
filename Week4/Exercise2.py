#Ex2
import pandas as pd
file = "iris.txt"
columns = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'class']
df = pd.read_csv(file, names = columns)

print(df)

# Calculate the statistical description of sepal_length, sepal_width, petal_length and petal_width by type of flower.
dfGrouped = df.groupby("class", as_index = True)
print("---statistical description of sepal_length: \n", dfGrouped['sepal_length'].describe())
print("---statistical description of sepal_width: \n", dfGrouped['sepal_width'].describe())
print("---statistical description of petal_length: \n", dfGrouped['petal_length'].describe())
print("---statistical description of petal_width: \n", dfGrouped['petal_width'].describe())

#○	For each type of flower, normalize its sepal_length and petal_length by replacing the outer line 50% distance to mean (i.e., x = (x + mean)/2 ). You can choose a distance to define outer line.
#Ko hiểu đề bài nói gì