#Ex3
import pandas as pd
import numpy as np
period = 50
date = pd.date_range(start = '2018-01-31', periods = period, freq = 'M')  #periods -> số lượng phần tử , freq = 'M' -> ngày cuối tháng
#print(date.strftime("%Y-%m-%d").tolist())

df = pd.DataFrame({'month': date,
                   'sale': np.random.randint(10, 30, period)})
#print(df)

#Thêm cột mới 'Year'
df['year'] = date.strftime("%Y")
print(df)
print('--------------------------')
#Calculate the sum of sale by year.
dfGrouped = df.groupby("year", as_index = True)
print("Calculate the sum of sale by year:\n", dfGrouped['sale'].sum())


#○	OPTIONAL: You can change freq=’D’ and periods=200 then calculate the sum of sale by month.
print('--------------------------')
period2 = 200
date2 = pd.date_range(start = "2018-01-31", periods = period2, freq = 'D') #freq = 'D' -> ngày trong tháng
df2 = pd.DataFrame({'day': date2,
                    'sale': np.random.randint(10,30, period2)})

#calculate the sum of sale by month.
df2['month'] = date2.strftime("%m")
print(df2)
df2Grouped = df2.groupby('month', as_index = True)
print('Calculate the sum of sale by month:\n', df2Grouped['sale'].sum())