# Ex4
import numpy as np
import pandas as pd
import random as rd

# create list ids
ids = []
for i in range(1, 21):
    ids.append('id' + str(i))

print(ids)


def randomeUniqueList(ids):
    l = []
    for i in rd.sample(range(20), 10):  # Xài randome.sample(range)
        l.append(ids[i])
    return l


S1 = pd.Series(randomeUniqueList(ids))
print('S1 is ------------------:\n', S1)
S2 = pd.Series(randomeUniqueList(ids))
print('S2 is ------------------:\n', S2)
S3 = pd.Series(np.random.randint(-100, 101, 10))
print('S3 is ------------------:\n', S3)
S4 = pd.Series(np.random.randint(-100, 101, 10))
print('S4 is ------------------:\n', S4)

print('--------------------------')
# ○	Build 2 DataFrame from those Series, df1 is a concatenation of S1, S3 and df2 is the concatenation of S2, S4.
df1 = pd.concat([S1, S3], axis=1)  # Nối theo index, giữ và hiển thị index cũ
df1.columns = ['key', 'data1']
print(df1)
print('--------------------------')
df2 = pd.concat([S2, S4], axis=1)  # Nối theo index, giữ và hiển thị index cũ
df2.columns = ['key', 'data2']
print(df2)

# ○	Execute a join on those two dataframe by using merge() function to have a DataFrame that share the common ids.
print('--------------------------')
df3 = df1.merge(df2, left_on='key', right_on='key', how='inner')
print(df3)