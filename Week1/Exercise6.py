#khai báo hàm tính tổng hai sô
def sum(x,y):
    sum = x + y
    if sum in range(15,20):
        return 20
    else:
        return sum

print(sum(10,6))     #20  do 15 <= sum <=20
print(sum(10,2))     #12  do sum < 15
print(sum(10,12))    #22  do sum > 20