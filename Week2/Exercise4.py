# nhập số nguyên n
def enterNumber(prompt, reminder="Please enter an integer number, ex 5"):
    while True:
        try:
            number = int(input(prompt))
            return number
        except ValueError:
            print(reminder)


# tạo ngẫu nhiên list n phần tử trên khoảng [start, end]
import random


def rand(start, end, n):
    randomList = []
    for i in range(n):
        randomList.append(random.randint(start, end))
    return randomList


def frequenceCount(randomList):
    d = {x: randomList.count(x) for x in randomList}
    for key, value in d.items():
        print(key, 'appears', value, 'times')


# Manually calculate
def maxCal(randomeList):
    maxValue = randomeList[0]
    for x in randomeList:
        if (maxValue < x):
            maxValue = x
    return maxValue


def minCal(randomeList):
    minValue = randomeList[0]
    for x in randomeList:
        if (minValue > x):
            minValue = x
    return minValue


def sumCal(randomList):
    sum = 0
    for x in randomList:
        sum += x
    return sum


def meanCal(randomList, n):
    return sum(randomList) / n


# Phương sai - variance
# Độ lêch chuẩn - standard deviation
import math


def varianceCal(randomList, n):  # return variance , deviation
    mean = meanCal(randomList, n)
    variance = sum((x - mean) ** 2 for x in randomList) / (n - 1)
    return variance, math.sqrt(variance)


# Remove duplicates from the list
def removeDuplicate(randomList):
    return list(set(randomList))



#-----------------Main-----------------------
n = enterNumber('Enter an integer number, ex 5:')
randomList = rand(1, 10, n)
print(randomList)
frequenceCount(randomList)

print("Max is:", maxCal(randomList))
print("Min is:", minCal(randomList))
print("Mean is", meanCal(randomList, n))
variance, deviation = varianceCal(randomList, n)
print("VarianceCal is {}, and standard deviation is {}".format(variance, deviation))

print("List after removing duplicates:", removeDuplicate(randomList))
