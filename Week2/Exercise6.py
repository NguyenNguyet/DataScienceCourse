#Exercise 6: Write a python script to get sum of column of matrix N x M
M = [[1, 2, 3, 4, 5],
     [3, 4, 2, 5, 6],
     [1, 6, 3, 2, 5]]

l = [sum(r[i] for r in M) for i in range(len(M[0]))]
print(l)