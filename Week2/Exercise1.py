# Bai 1

def enterListNumbers(prompt, reminder="Please enter just a list of number, ex: 12, 32, 31, 12"):
    while True:
        try:
            inputs = input(prompt).split(", ")
            intList = [int(x) for x in inputs]
            return intList
        except ValueError:
            print(reminder)


intList = enterListNumbers('Enter a list of string (ex: 12, 32, 31, 12):')
listEven, listOdd = [], []
for number in intList:
    if number % 2 == 0:
        listEven.append(number)
    else:
        listOdd.append(number)
print('list input of numbers:', intList)
print('list of even number:', listEven)
print('list of odd number:', listOdd)
