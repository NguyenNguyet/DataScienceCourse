#Exercise 3
s = 'the output that tells you if the object is the type you think or not'
#sort the words of this string by alphabet.
l = s.split()
copyL1 = l[:]
copyL1.sort()
print(' '.join(copyL1))

#upper case all the first character of the words
copyL2 = [x.title() for x in l[:]]
print(' '.join(copyL2))
