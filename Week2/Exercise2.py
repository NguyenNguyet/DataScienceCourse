#Bai 2
def sort_sum(x):
    sum = 0
    for item in x:
        if (type(item) is int) or (type(item) is float):
            sum += item
    return sum

l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]
l.sort(key = sort_sum)
print('After sort by sum of int or float items:', l)