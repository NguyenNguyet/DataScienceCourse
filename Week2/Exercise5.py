listX = [round(x * 0.1, 1) for x in range(1, 101)]
print("X =", listX)

listY = [(x ** 2) / (4 * x) for x in listX]
print("Y =", listY)